const express = require("express");
const fs = require("fs");
const path = require("path");
const { format } = require("date-fns");
const logEvents = require("./logEvents");

const app = express();
const port = 8080;
const storagePath = "./storage";
const EventEmitter = require("events");

class MyEmitter extends EventEmitter {}

const myEmitter = new MyEmitter();

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.post("/api/files", (req, res) => {
  req.on("data", (chunk) => {
    const data = JSON.parse(chunk);
    const extension = path.extname(data.filename);

    if (data.filename && data.content) {
      switch (extension) {
        case ".txt":
        case ".log":
        case ".xml":
        case ".json":
        case ".js":
        case ".yaml":
          // write in file by path
          // data.content
          fs.writeFile(
            path.join(storagePath, data.filename),
            data.content,
            () => {
              res.status(200).send({ message: "File created successfully" });
              myEmitter.emit(
                "log",
                `${req.url}\t${req.method} "Create file complete" `,
                "reqLog.txt"
              );
            }
          );
          break;
        default:
          res
            .sendStatus(400)
            .send({ message: "Please specify 'content' parameter" });
          myEmitter.emit(
            "log",
            `${req.url}\t${req.method} "can't not create file"`,
            "reqLog.txt"
          );

        // fs.writeFile()
      }
    } else {
      res.status(400).send({ message: "Please specify 'content' parameter" });
    }
  });
});

app.get("/api/files", (req, res) => {
  // get list of saved files and send the list
  myEmitter.emit(
    "log",
    `${req.url}\t${req.method} "Get list files"`,
    "reqLog.txt"
  );

  fs.readdir(storagePath, (err, files) => {
    res.status(200).send({ message: "Success", files });
    res.end();
  });
  // res.send(array);
});

app.get("/api/files/:filename", (req, res) => {
  const fileName = req.params;
  const dateTime = new Date();

  myEmitter.emit(
    "log",
    `${req.url}\t${req.method} "file detailed info"`,
    "reqLog.txt"
  );
  if (fs.existsSync(path.join(storagePath, fileName.filename))) {
    fs.readFile(
      path.join(storagePath, fileName.filename),
      "utf8",
      (err, data) => {
        if (err) {
          console.error(err);
          return;
        }
        res.status(200).send({
          message: "Success",
          filename: fileName.filename,
          content: data,
          extension: fileName.filename.split(".").pop(),
          uploadedDate: dateTime.toISOString(),
        });
        res.end();
      }
    );
  } else {
    res
      .status(400)
      .send(`"message": "No file with ${fileName.filename} filename found"`);
    res.end();
  }
});

app.listen(port, () => {
  if (!fs.existsSync(storagePath)) {
    fs.mkdirSync(storagePath);
  }
  console.log(`Example app listening on port ${port}`);
});

myEmitter.on("log", (msg) => logEvents(msg));
